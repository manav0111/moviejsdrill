// // Group movies based on genre. Priority of genres in case of multiple genres present are: drama > sci-fi > adventure > thriller > crime
const favouritesMovies = require("./MoviesData");

const PriorityGenre = {
  drama: 5,
  "sci-fi": 4,
  adventure: 3,
  thriller: 2,
  crime: 1,
};

const result = {};

Object.keys(favouritesMovies).map((key) => {
  const genreArray = favouritesMovies[key].genre;

  let genre = genreArray[0];

  for (let index = 1; index < genreArray.length; index++) {
    if (PriorityGenre[genre] > PriorityGenre[genreArray[index]]) {
      continue;
    } else {
      genre = genreArray[index];
    }
  }

  if (!result[genre]) {
    result[genre] = {};
  }

  result[genre][key] = favouritesMovies[key];
});

console.log(result);

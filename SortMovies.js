// Q.4 Sort movies (based on IMDB rating) if IMDB ratings are same, compare totalEarning as the secondary metric.
const favouritesMovies=require('./MoviesData');

let result=[];

result=Object.keys(favouritesMovies).sort((a,b)=>{

    const ratingComparison=favouritesMovies[b].imdbRating-favouritesMovies[a].imdbRating;

    if(ratingComparison===0)
    {
        let EarningA=favouritesMovies[a].totalEarnings;
        EarningA=Number(EarningA.slice(1).slice(0,EarningA.length-2));
        let EarningB=favouritesMovies[b].totalEarnings;
        EarningB=Number(EarningB.slice(1).slice(0,EarningB.length-2));
        
        return EarningB-EarningA;
    }

    return ratingComparison;
    
   
}).map((key)=>{
    return {[key]: favouritesMovies[key]};
})

console.log(result);